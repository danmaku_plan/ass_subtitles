# 介绍

视频弹幕(ass)备份

# 平台
- bilibili: https://comment.bilibili.com/{cid}.xml

- 腾讯视频: https://dm.video.qq.com/barrage/base/{vid}

  腾讯按时间(30秒)分弹幕，后续弹幕取```segment_index```字段,```base```换成```segment```

# 转换
xml转换ass: https://danmubox.github.io/convert
